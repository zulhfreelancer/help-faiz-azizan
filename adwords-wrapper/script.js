$(function() {
  $("#results").hide()

  $("#seeds-form").submit(function(event) {
    var modifiedBroadMatchResult = ""
    var phraseMatchResult = ""
    var exactMatchResult = ""

    event.preventDefault()
    var seeds = $("#seeds").val().split("\n")
    for (i = 0; i < seeds.length; i++) {
        var line = seeds[i]
        if (line == "") {
            continue // skip if empty
        }
        
        var modifiedBroadMatch = ""
        var wordsInLine = line.split(" ")
        for (j = 0; j < wordsInLine.length; j++) {
            var word = wordsInLine[j]
            var modifiedBroadMatchWord = prependString(word, "+")
            modifiedBroadMatch += modifiedBroadMatchWord + " "
        }
        var phraseMatch = getPhraseMatch(line)
        var exactMatch = getExactMatch(line)

        modifiedBroadMatchResult += modifiedBroadMatch + "<br>"
        phraseMatchResult += phraseMatch + "<br>"
        exactMatchResult += exactMatch + "<br>"
    }

    $("#modified-broad-match-result").html(modifiedBroadMatchResult)
    $("#phrase-match-result").html(phraseMatchResult)
    $("#exact-match-result").html(exactMatchResult)
    $("#results").show()
  })
})

function prependString(input, symbol) {
    return symbol + input
}

function appendString(input, symbol) {
    return input + symbol
}

function getPhraseMatch(input) {
    symbol = "\""
    prepended = prependString(input, symbol)
    appended = appendString(prepended, symbol)
    return appended
}

function getExactMatch(input) {
    prepended = prependString(input, "[")
    appended = appendString(prepended, "]")
    return appended
}
