## Demo

- https://zulhfreelancer.gitlab.io/help-faiz-azizan/tulis-ads/
- https://zulhfreelancer.gitlab.io/help-faiz-azizan/adwords-wrapper/

## Resources

- https://tulisads.online/copy-block
- https://www.adwordswrapper.com

## Useful Tools

### Live Reload

```
$ npm install --global livereloadx
$ cd /path/to/project
$ livereloadx --static --port 3000 .
```

The project will available at http://localhost:3000 with live reload enabled. Meaning, you don't have to manually reload the page everytime you change the files in your static website project. To stop it, hit `Ctrl + C` shortcut.
