$(function() {
    var defaultHasil = "Hasil Selepas Pakai Produk"
    var defaultPasaran = "Pasaran Sasaran Anda"
    var defaultMasalah = "Masalah Masalah Produk Anda Selesaikan"
    $(".hasil").text(defaultHasil)
    $(".pasaran").text(defaultPasaran)
    $(".masalah").text(defaultMasalah)

    $(".form-input").keyup(function(){
        var hasil = $("#input-hasil").val()
        var pasaran = $("#input-pasaran").val()
        var masalah = $("#input-masalah").val()

        if (hasil != "") {
            $(".hasil").text(hasil)
        } else {
            $(".hasil").text(defaultHasil)
        }

        if (pasaran != "") {
            $(".pasaran").text(pasaran)
        } else {
            $(".pasaran").text(defaultPasaran)
        }

        if (masalah != "") {
            $(".masalah").text(masalah)
        } else {
            $(".masalah").text(defaultMasalah)
        }
    })
})
